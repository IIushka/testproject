// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "CharacterInterface.generated.h"

/**
 * 
 */
UINTERFACE(MinimalAPI)
class UCharacterInterface : public UInterface
{
	GENERATED_BODY()

};

class ICharacterInterface
{
    GENERATED_BODY()

public:
	virtual void Rolling(float value);
	virtual float GetMaxWalkSpeed();
};
