// Fill out your copyright notice in the Description page of Project Settings.


#include "GameCharacter.h"
#include "GameFramework/SpringArmComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Camera/CameraComponent.h"
#include "Net/UnrealNetwork.h"


AGameCharacter::AGameCharacter()
{
	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = true;
	bUseControllerRotationRoll = false;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 400.f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Network
	bReplicates = true;
}

void AGameCharacter::Rolling(float value)
{
	float rate = RollingMontage->GetPlayLength() / value;	// ������������ �������� �������� � ����������� �� ������� �������

	PlayAnim_OnServer(RollingMontage, rate);
}

float AGameCharacter::GetMaxWalkSpeed()
{
	return GetCharacterMovement()->GetMaxSpeed();
}

void AGameCharacter::PlayAnim_OnServer_Implementation(UAnimMontage* animation, float rate)
{
	PlayAnim_Multicast(animation, rate);
}

void AGameCharacter::PlayAnim_Multicast_Implementation(UAnimMontage* animation, float rate)
{
	PlayAnimMontage(animation, rate);
}