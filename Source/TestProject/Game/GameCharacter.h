// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "TestProject/Interfaces/CharacterInterface.h"
#include "GameCharacter.generated.h"

UCLASS()
class TESTPROJECT_API AGameCharacter : public ACharacter, public ICharacterInterface
{
	GENERATED_BODY()

public:
	AGameCharacter();
	
private:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class USpringArmComponent* CameraBoom;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UCameraComponent* FollowCamera;

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim Montages")
		UAnimMontage* RollingMontage = nullptr;

	// Interfaces
	virtual void Rolling(float value) override;
	virtual float GetMaxWalkSpeed();

	// Network
	UFUNCTION(Server, Reliable)
		void PlayAnim_OnServer(UAnimMontage* animation, float rate);
	UFUNCTION(NetMulticast, Reliable)
		void PlayAnim_Multicast(UAnimMontage* animation, float rate);
};
