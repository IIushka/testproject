// Fill out your copyright notice in the Description page of Project Settings.


#include "GamePlayerController.h"
#include "TestProject/Interfaces/CharacterInterface.h"

AGamePlayerController::AGamePlayerController()
{
	bShowMouseCursor = false;
}

void AGamePlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	InputComponent->BindAxis(TEXT("MoveForward"), this, &AGamePlayerController::MoveForward);
	InputComponent->BindAxis(TEXT("MoveRight"), this, &AGamePlayerController::MoveRight);

	InputComponent->BindAxis("Turn", this, &AGamePlayerController::AddYaw);
	InputComponent->BindAxis("LookUp", this, &AGamePlayerController::AddPitch);

	InputComponent->BindAction("Rolling", IE_Pressed, this, &AGamePlayerController::StartRolling);

	InputComponent->BindAction("EnterChat", IE_Pressed, this, &AGamePlayerController::EnterChat);
	InputComponent->BindAction("LeaveChat", IE_Pressed, this, &AGamePlayerController::LeaveChat);
}

void AGamePlayerController::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (rolling)	// ���� ���������� �������
		RollingMovement();
}

void AGamePlayerController::MoveForward(float value)
{
	if (value != 0.0f && !rolling)
	{
		// ��������� ������� �������
		const FRotator Rotation = GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);

		GetPawn()->AddMovementInput(Direction, value);	// �������� �� ��������� �������
	}
}

void AGamePlayerController::MoveRight(float value)
{
	if (value != 0.0f && !rolling)
	{
		// ��������� ������� ����������� �������
		const FRotator Rotation = GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);

		GetPawn()->AddMovementInput(Direction, value);	// �������� �� ��������� �������
	}
}

void AGamePlayerController::AddYaw(float value)
{
	AddYawInput(value);
}

void AGamePlayerController::AddPitch(float value)
{
	AddPitchInput(value);
}

void AGamePlayerController::StartRolling()
{
	if(!rolling)
	{
		float timeRolling = 1.f;
		ICharacterInterface* characterInterface = Cast<ICharacterInterface>(GetPawn());
		if (characterInterface) 
		{
			// ������ ������� ������� � ����������� �� ��������� � ��������
			timeRolling = disatanceRolling / characterInterface->GetMaxWalkSpeed();

			// ��������� ������ �������
			characterInterface->Rolling(timeRolling);
		}

		rolling = true;
		GetPawn()->bUseControllerRotationYaw = false;	// ���������� �������� ��������� �� �������

		// ��������� ������� ������� ��� �������
		FRotator Rotation = GetControlRotation();
		FRotator YawRotation(0, Rotation.Yaw, 0);
		forwardDirection = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);

		// ������ ������� �������
		GetWorld()->GetTimerManager().SetTimer(TimerRolling, this, &AGamePlayerController::EndRolling, timeRolling, false);
	}
}

void AGamePlayerController::EndRolling()
{
	GetWorld()->GetTimerManager().ClearTimer(TimerRolling);
	rolling = false;
	GetPawn()->bUseControllerRotationYaw = true;	// ��������� �������� ��������� �� �������
}

void AGamePlayerController::RollingMovement()
{
	GetPawn()->AddMovementInput(forwardDirection, 1.f);	// �������� �� ��������� �������
}
