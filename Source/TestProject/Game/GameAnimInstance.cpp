// Fill out your copyright notice in the Description page of Project Settings.


#include "GameAnimInstance.h"
#include "Kismet/KismetMathLibrary.h"

void UGameAnimInstance::NativeUpdateAnimation(float DeltaSeconds)
{
	Super::NativeUpdateAnimation(DeltaSeconds);

	AActor* OwningActor = GetOwningActor();	// �������� ������ �� ������ ��������

	if (OwningActor)
	{
		// ��������� �������� ������������
		speed = UKismetMathLibrary::VSize(OwningActor->GetVelocity());
	}
}
