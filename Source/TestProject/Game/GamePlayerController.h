// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "GamePlayerController.generated.h"

/**
 * 
 */
UCLASS()
class TESTPROJECT_API AGamePlayerController : public APlayerController
{
	GENERATED_BODY()
	AGamePlayerController();

protected:
	virtual void SetupInputComponent() override;
	virtual void Tick(float DeltaSeconds) override;

public:	
	void MoveForward(float value);
	void MoveRight(float value);
	void AddYaw(float value);
	void AddPitch(float value);
	void StartRolling();
	void EndRolling();
	void RollingMovement();

	UFUNCTION(BlueprintImplementableEvent, Category = "Chat")
		void EnterChat();
	UFUNCTION(BlueprintImplementableEvent, Category = "Chat")
		void LeaveChat();

	// Variable
	float disatanceRolling = 600.f;	// ��������� �������
	bool rolling = false;
	FVector forwardDirection;

	FTimerHandle TimerRolling;
};
